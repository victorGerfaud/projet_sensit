<?php

use Faker\Generator as Faker;

$factory->define(App\Device::class, function (Faker $faker) {
    return [
    	'auth' => $faker->sha256,
    	'user_id' => '1',
    	'name' => 'Device',
    	'sigfox_id' => '86B7E4',
    	'remote_id' => '86B7E4',
    	'remote_name' => 'sensit_salon',
    ];
});
