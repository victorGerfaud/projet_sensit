<?php

use Faker\Generator as Faker;

$factory->define(App\Battery_measure::class, function (Faker $faker) {
	return [
		'device_id' => '1',
		'message_id' => $faker->unique()->randomNumber,
		'value' => $faker->randomFloat( 2, 0, 40),
		'received_at' => $faker->dateTimeThisYear(),
	];
});
