<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemperatureMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temperature_measures', function (Blueprint $table) {

            /**
            * Set the table engine
            * @var string
            */
            $table->engine = 'InnoDB';

            /**
             * Create columns
             */
            $table->increments('id');
            $table->unsignedInteger('device_id');
            $table->integer('message_id')->unique();
            $table->decimal('value');
            $table->timestamp('received_at');
            $table->timestamps();
            $table->softDeletes();

            /**
             * Create foreign key
             */
            $table->foreign('device_id')->references('id')->on('devices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temperature_measures');
    }
}
