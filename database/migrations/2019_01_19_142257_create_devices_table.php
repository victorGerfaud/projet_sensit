<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {

            /**
             * Create columns
             */
            $table->increments('id');
            $table->string('auth', 128)->unique();
            $table->unsignedInteger('user_id');
            $table->string('name', 128);
            $table->string('sigfox_id', 128)->unique();
            $table->string('remote_id', 32)->unique();
            $table->string('remote_name', 64);
            $table->timestamps();
            $table->softDeletes();

            /**
             * Create foreign key
             */
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
