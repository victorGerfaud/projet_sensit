<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create()->each(function($user, $key){

        	$user->name = 'VictorG';
        	$user->email = 'victor@gerfaud.fr';
        	$user->password = Hash::make('sensit_2019');

            $user->save();
        });
    }
}
