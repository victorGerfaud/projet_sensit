<?php

use Illuminate\Database\Seeder;

class TemperatureMeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Temperature_measure::class, 200)->create()->each(function($temperature_measure, $key){
            $temperature_measure->save();
        });
    }
}
