<?php

use Illuminate\Database\Seeder;

class BatteryMeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Battery_measure::class, 100)->create()->each(function($battery_measure, $key){
            $battery_measure->save();
        });
    }
}
