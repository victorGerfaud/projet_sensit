<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class ManageAvatar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file;
    public $formats;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $file)
    {
        $this->file = $file;
        $this->formats = [50, 150, 200, 500];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        //Iterate over formats to create multiple resized duplicates
        foreach($this->formats as $format){
            $manager = new ImageManager(['driver' => 'gd']);
            $manager->make($this->file)
            ->fit($format, $format)
            ->save(public_path('avatars') . "/" . pathinfo($this->file, PATHINFO_FILENAME) . "_{$format}x{$format}.jpg");
        }

        // unlink(storage_path('app/avatars/'.$this->file));

        // \File::delete($this->file);

    }
}