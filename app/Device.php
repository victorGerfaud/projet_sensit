<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

class Device extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    /**
    * The child model instances that should be softdeleted.
    *
    * @var array
    */

    protected $cascadeDeletes = ['temperatureMeasures', 'batteryMeasures'];

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function temperatureMeasures(){
        return $this->hasMany('App\Temperature_measure');
    }

    public function batteryMeasures(){
        return $this->hasMany('App\Battery_measure');
    }
}
