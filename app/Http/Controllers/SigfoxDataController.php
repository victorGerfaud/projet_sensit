<?php

namespace App\Http\Controllers;

use App\Device;
use App\Temperature_measure;
use App\Battery_measure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SigfoxDataController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get the data array
        $data = Input::all();

        //Get the device
        $device = Device::where('sigfox_id', $data['sigfoxId'])->firstOrFail();

        //Create the temperature measure
        $newTemperatureMeasure = new Temperature_measure;
        $newTemperatureMeasure->message_id = $data['messageId'];
        $newTemperatureMeasure->value = $data['value'];
        $newTemperatureMeasure->received_at = $data['receivedAt'];

        //Create the battery measure
        $newBatteryMeasure = new Battery_measure;
        $newBatteryMeasure->message_id = $data['messageId'];
        $newBatteryMeasure->value = $data['batteryIndicator'];
        $newBatteryMeasure->received_at = $data['receivedAt'];

        //Associate measures to target device
        $newTemperatureMeasure->device()->associate($device);
        $newBatteryMeasure->device()->associate($device);

        //Save data in database
        $newTemperatureMeasure->save();
        $newBatteryMeasure->save();
        $device->save();

        return 'Data added';
    }
}
