<?php

namespace App\Http\Controllers;

use App\Device;
use App\temperature_measure;
use Illuminate\Http\Request;

class TemperatureMeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(String $deviceAuth)
    {
        //Getthe the temperature measures for the target device
        $temperatureMeasures = Device::where('auth', $deviceAuth)->firstOrFail()->temperatureMeasures()->paginate(10);

        // load the view and pass packets
        return view('temperature.index', compact('temperatureMeasures'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\temperature_measure  $temperature_measure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temperature_measure $temperature_measure)
    {
        //
    }
}
