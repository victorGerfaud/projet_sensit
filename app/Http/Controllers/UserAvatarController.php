<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAvatarRequest;
use App\Jobs\ManageAvatar;

class UserAvatarController extends Controller
{

    public function edit(){

        $user = Auth::user();

        return view('user.edit', compact('user'));
    }

    /**
     * Update the avatar for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(UserAvatarRequest $request)
    {
        //Form validator
        $validated = $request->validated();

        //Get current user
        $user = Auth::user();

        if($request->hasFile('avatar')){

            // $job = new ManageAvatar($request->file('avatar'));

            //Treatment in image queue
            // $this->dispatch($job->onQueue('image'));

            $path = $request->file('avatar')->store('avatars');

            $user->avatar = $path;
            $user->save();
        }

        return back()
        ->with('success','You have successfully upload image.');
    }
}