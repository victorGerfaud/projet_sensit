<?php

namespace App\Http\Controllers;

use App\User;
use App\Device;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\DeviceRequest;

class DeviceController extends Controller
{

    public function __construct()
    {
        //Oblige l'utilisateur à être authentifié pour accéder au formulaire
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Check authorization
        $this->authorize('view', Device::class);

        //Get the current user
        $user = \Auth::user();

        //Get the devices of the user
        $devices = $user->devices()->paginate(5);

        // load the view and pass devices
        return view('device.index', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check authorization
        $this->authorize('create', Device::class);

        return view('device.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeviceRequest $request)
    {
        //Check authorization
        $this->authorize('create', Device::class);

        //Form validator
        $validated = $request->validated();

        //Create the device instance
        $device = new device;

        //Fields management
        $device->auth = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 40);
        $device->name = $request->name;
        $device->sigfox_id = $request->sigfox_id;
        $device->remote_id = '';
        $device->remote_name = '';

        //Relationships management
        $device->user()->associate(Auth::id());

        //Save the device instance
        $device->save();

        //Flash message
        flash('Device créé!!')->success();

        return redirect('devices/' . $device->auth);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function show(String $auth)
    {

        //Get the device
        $device = Device::where('auth', $auth)->firstOrFail();

        //Check authorization
        $this->authorize('view', $device);

        return view('device.show', compact('device'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit(String $auth)
    {
        //Get the device
        $device = Device::where('auth', $auth)->firstOrFail();

        //Check authorization
        $this->authorize('update', $device);

        return view('device.edit', compact('device'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function update(DeviceRequest $request, String $auth)
    {
        //Get the device
        $device = Device::where('auth', $auth)->firstOrFail();

        //Check authorization
        $this->authorize('update', $device);

        //Form validator
        $validated = $request->validated();

        //Fields management
        $device->auth = $auth;
        $device->name = $request->name;
        $device->sigfox_id = $request->sigfox_id;
        $device->remote_id = '';
        $device->remote_name = '';

        //Relationships management
        $device->user()->associate(Auth::id());

        //Save the device instance
        $device->save();

        //Flash message
        flash('Device créé!!')->success();

        return redirect('devices/' . $device->auth);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy(String $auth)
    {
        //Get the device
        $device = Device::where('auth', $auth)->firstOrFail();

        //Check authorization
        $this->authorize('delete', $device);

        //Delete the device
        $device->delete();

        return redirect('devices')->with('success','Le device a été supprimé');
    }
}
