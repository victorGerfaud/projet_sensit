<?php

namespace App\Http\Controllers;

use App\Device;
use App\battery_measure;
use Illuminate\Http\Request;

class BatteryMeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(String $deviceAuth)
    {
        //Getthe the battery measures for the target device
        $batteryMeasure = Device::where('auth', $deviceAuth)->firstOrFail()->batteryMeasures()->first();

        // load the view and pass packets
        return view('battery.show', compact('batteryMeasure'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\battery_measure  $battery_measure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Battery_measure $battery_measure)
    {
        //
    }
}
