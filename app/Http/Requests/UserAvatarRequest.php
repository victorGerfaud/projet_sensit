<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'file.required' => 'Choisissez une image',
            'file.image' => 'Votre avatar doit être une image',
            'file.mimes' => 'Les formats autorisés pour votre avatar sont: jpeg,png,jpg,gif,svg',
            'file.max' => 'La taille maximale de votre avatar doit être de 2048x2048',
        ];
    }
}
