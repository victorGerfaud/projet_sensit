<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:128',
            'sigfox_id' => 'required|string|max:128',
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'name.required' => 'Veuillez choisir un nom pour votre device',
            'name.string' => 'Le nom du device doit être une chaîne de caractères',
            'name.max' => 'Le nom du device doit être d\'une taille de 255 charactères maximum',
            'sigfox_id.required' => 'Veuillez choisir un ID sigfox pour votre device',
            'sigfox_id.string' => 'Votre ID sigfox doit être une chaîne de caractères',
            'sigfox_id.128' => 'Votre ID sigfox doit être d\'une taille de 255 charactères maximum',
        ];
    }
}
