<?php

use Illuminate\Http\Request;
use App\Battery_measure;
use App\Temperature_measure;
use App\Device;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

//Get the temperature measures for a specific device
Route::get('v1/devices/{sigfoxID}/temperaturemeasures', function(String $sigfoID) {

	//Get the target device
	$device = Device::where('sigfox_id', $sigfoID)->first();

	//If no device, returns 204 no content
	if(!$device){
		return Response::make("", 204);
	}

    // If the Content-Type and Accept headers are set to 'application/json',
   	// this will return a JSON structure.
	return $device->temperatureMeasures()->get()->toArray();
});

//Get the battery measures for a specific device
Route::get('v1/devices/{sigfoxID}/batterymeasures', function(String $sigfoID) {

	//Get the target device
	$device = Device::where('sigfox_id', $sigfoID)->firstOrFail();

	//If no device, returns 204 no content
	if(!$device){
		return Response::make("", 204);
	}

    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure.
	return Temperature_measure::all();
});

Route::post('temperaturemeasure', 'SigfoxDataController@store');