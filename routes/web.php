<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resources([
	'devices' => 'DeviceController',
]);

Route::get('devices/{deviceAuth}/temperature', 'TemperatureMeasureController@index')->name('temperature.index')->middleware('auth');
Route::get('devices/{deviceAuth}/battery', 'BatteryMeasureController@show')->name('battery.show')->middleware('auth');

Route::get('/profile', 'UserAvatarController@edit')->name('profile');
Route::post('profile', 'UserAvatarController@update');
