@extends('layouts.main')

@section('content')
<div style="display: flex;">
	<div>
		<h1>Mes devices</h1>
	</div>
	<div id="main-button-container">
		<form class="main-button" action="{{ url('/devices/' . $device->auth . '/temperature') }}">
			{{method_field('GET')}}
			{!! csrf_field() !!}
			<button class="btn btn-small btn-secondary mb-1 col-12" type="submit">Suivre ma température</button>
		</form>
		<form class="main-button" action="{{ url('/devices/' . $device->auth . '/battery') }}">
			{{method_field('GET')}}
			{!! csrf_field() !!}
			<button class="btn btn-small btn-secondary mb-1 col-12" type="submit">Suivre ma batterie</button>
		</form>
	</div>
</div>
@include('flash::message')
<div class="jumbotron text-center">
	<h2></h2>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nom</th>
				<th>ID Sigfox</th>
				<th>ID distant</th>
				<th>Nom distant</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> {{ $device->name }}</td>
				<td> {{ $device->sigfox_id }}</td>
				<td> {{ $device->remote_id }}</td>
				<td> {{ $device->remote_name }}</td>
				<td>
					<form action="{{ url('/devices/'.$device->auth.'/edit') }}" method="post">
						{{method_field('GET')}}
						{!! csrf_field() !!}
						<button class="btn btn-small btn-info mb-1 w-50" type="submit">Modifier le device</button>
					</form>
					<form action="{{ url('/devices', [$device->auth]) }}" method="post">
						{{method_field('DELETE')}}
						{!! csrf_field() !!}
						<button class="btn btn-small btn-outline-danger w-50" type="submit">Supprimer le device</button>
					</form>
				</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection
@section('pagespecificscripts')
<script type="text/javascript" charset="utf-8">
	$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection
