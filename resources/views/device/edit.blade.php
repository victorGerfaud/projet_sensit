@extends('layouts.main')

@section('css')
@endsection
@section('content')

<!-- Gestion des erreurs -->
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif

{{-- Form --}}
{!! Form::open(['url' => route('devices.update', $device->auth), 'method' => 'PUT']) !!}
{!!  Form::token() !!}

<div class="form-group" >

    {!! Form::label('name', 'Nom de votre device') !!}
    {!! Form::input('text', 'name', $device->name ? : null, ['class' => 'form-control']) !!}

    {!! Form::label('sigfox_id', 'ID Sigfox'); !!}
    {!! Form::input('text', 'sigfox_id', $device->sigfox_id ? : null, ['class' => 'form-control']) !!}

    {!! Form::submit('Modifier mon device', ['class' => 'btn btn-primary float-right mt-3']) !!}

</div>

{!! Form::close() !!}

@endsection
@section('pagespecificscripts')
@stop
