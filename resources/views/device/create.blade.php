@extends('layouts.main')

@section('css')
@endsection
@section('content')

<!-- Gestion des erreurs -->
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif

{!! Form::open(['url' => route('devices.store'), 'method' => 'POST', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
{!!  Form::token() !!}

<div class="form-group" >

    {!! Form::label('name', 'Nom de votre device') !!}
    {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}

    {!! Form::label('sigfox_id', 'ID Sigfox'); !!}
    {!! Form::input('text', 'sigfox_id', null, ['class' => 'form-control']) !!}

    {!! Form::submit('Ajouter mon device', ['class' => 'btn btn-primary float-right mt-3']) !!}

</div>

{!! Form::close() !!}

@endsection
@section('pagespecificscripts')
@stop
