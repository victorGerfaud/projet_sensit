@extends('layouts.main')

@section('content')
<div style="display: flex;">
	<div>
		<h1>Mes devices</h1>
	</div>
	<div style="margin: 5px 0 0 40px; width: 150px;">
		<form action="{{ url('/devices/create') }}">
			{{method_field('GET')}}
			{!! csrf_field() !!}
			<button class="btn btn-small btn-secondary mb-1 col-12" type="submit">Créer un nouveau device</button>
		</form>
	</div>
</div>
@include('flash::message')
<div class="jumbotron text-center">
	<h2></h2>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nom</th>
				<th>ID Sigfox</th>
				<th>ID distant</th>
				<th>Nom distant</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($devices as $device)
			<tr>
				<td> {{ $device->name }}</td>
				<td> {{ $device->sigfox_id }}</td>
				<td> {{ $device->remote_id }}</td>
				<td> {{ $device->remote_name }}</td>
				<td>
					<form action="{{ url('/devices/'.$device->auth) }}">
						{{method_field('GET')}}
						{!! csrf_field() !!}
						<button class="btn btn-small btn-info mb-1 col-12" type="submit">Visualiser le device</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $devices->links() }}
</div>
@endsection
@section('pagespecificscripts')
<script type="text/javascript" charset="utf-8">
	$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection
