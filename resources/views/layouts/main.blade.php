@include('partials.header')

<body>
	<!-- main content -->
	<div id="app">
		@include('partials.navigation')
		<div class="flex-center position-ref pt-5">
			<div id='container-main'>
				<div class='centralizer'>
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</body>
@include('partials.footer')