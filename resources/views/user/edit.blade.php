@extends('layouts.main')

@section('content')
<h1>Modification utilisateur</h1>
@include('flash::message')
<div class="jumbotron text-center">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img src="{{ asset('/uploads/avatars/' . Auth::user()->avatar . '_150x150.jpg') }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
            <h2>Profil de {{ $user->name }}</h2>
            <form enctype="multipart/form-data" action="/profile" method="POST">
                <label>Modifier mon image de profil</label>
                <input type="file" name="avatar" style="margin-top: 20px;">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="pull-right btn btn-sm btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection
@section('pagespecificscripts')
<script type="text/javascript" charset="utf-8">
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection