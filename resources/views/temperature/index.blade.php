@extends('layouts.main')

@section('content')
<div style="display: flex;">
	<div>
		<h1>Mes mesures de température</h1>
	</div>
</div>
<div class="jumbotron text-center">
	<h2></h2>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>ID du message</th>
				<th>Température</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
			@foreach($temperatureMeasures as $temperatureMeasure)
			<tr>
				<td> {{ $temperatureMeasure->message_id }}</td>
				<td> {{ $temperatureMeasure->value }}</td>
				<td> {{ $temperatureMeasure->received_at }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $temperatureMeasures->links() }}
</div>
@endsection
@section('pagespecificscripts')
@endsection
