<nav class="navbar navbar-expand-md navbar-dark navbar-laravel">
	<div class="container">
		<a class="navbar-brand" href="{{ url('/welcome') }}">
			{{ config('app.name', 'Laravel') }}
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Left Side Of Navbar -->
			<ul class="navbar-nav mr-auto">
				@auth
				<li class="nav-item {{ Request::is('ads*') ? 'active' : '' }}">
					<a class="nav-link" href="{{ route('devices.index') }}">{{ __('Mes devices') }}</a>
				</li>
				@endauth
			</ul>

			<!-- Right Side Of Navbar -->
			<ul class="navbar-nav ml-auto">
				<!-- Authentication Links -->
				@guest
				<li class="nav-item">
					<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
				</li>
				<li class="nav-item">
					@if (Route::has('register'))
					<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
					@endif
				</li>
				@else
				<li class="nav-item">
					<img src="{{ asset('storage/app/' . Auth::user()->avatar) }}" style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;">
				</li>
				<li class="nav-item dropdown">
					<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
						{{ Auth::user()->name }} <span class="caret"></span>
					</a>
					{{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="{{ route('profile') }}"
						onclick="event.preventDefault();
						document.getElementById('change-user-avatar').submit();">
						{{ __('Changer mon image') }}
					</a> --}}
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="{{ route('profile') }}"
						onclick="event.preventDefault();
						document.getElementById('change-user-avatar').submit();">
						{{ __('Changer mon image') }}
					</a>
					<a class="dropdown-item" href="{{ route('logout') }}"
					onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
					{{ __('Logout') }}
				</a>

				<form id="change-user-avatar" action="{{ route('profile') }}" method="GET" style="display: none;">
					@csrf
				</form>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			</div>
		</li>
		@endguest
	</ul>
</div>
</div>
</nav>
