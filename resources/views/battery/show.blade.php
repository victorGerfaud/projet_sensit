@extends('layouts.main')

@section('content')
<div style="display: flex;">
	<div>
		<h1>Ma batterie</h1>
	</div>
</div>
<div class="jumbotron text-center">
	<h2></h2>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>ID du message</th>
				<th>Batterie</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td> {{ $batteryMeasure->message_id }}</td>
				<td> {{ $batteryMeasure->value }}</td>
				<td> {{ $batteryMeasure->received_at }}</td>
			</tr>
		</tbody>
	</table>
</div>
@endsection
@section('pagespecificscripts')
@endsection
